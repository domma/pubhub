use sqlx;
use sqlx::types::uuid::Uuid;
use sqlx::types::chrono::{DateTime, Utc};
use serde::Serialize;
use sqlx::postgres::PgPool;

pub async fn prepare_login(pool: &PgPool, email: &str) -> Uuid {
    sqlx::query!("select prepare_login($1)", &email)
        .fetch_one(pool)
        .await
        .unwrap()
        .prepare_login
        .unwrap()
}

#[derive(Serialize)]
pub struct Account {
    pub id: Uuid,
    pub primary_email_id: u32,
    pub created: DateTime<Utc>,
}

pub async fn execute_login(pool: &PgPool, uuid: Uuid) -> Result<Account,sqlx::Error> {
    sqlx::query_as_unchecked!(Account, "select * from execute_login($1)", &uuid)
        .fetch_one(pool)
        .await
}


#[derive(Serialize)]
pub struct LoginAttempt {
    pub id: i32,
    pub email_id: i32,
    pub created: DateTime<Utc>,
}

pub async fn get_login_attempts(pool: &PgPool) -> Vec<LoginAttempt> {
    sqlx::query_as_unchecked!(LoginAttempt, "select * from login_attempts")
        .fetch_all(pool)
        .await
        .unwrap()
}

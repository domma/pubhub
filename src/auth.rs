use actix_web::{get, post, web, dev, Error, HttpRequest, Responder, HttpResponse, FromRequest};
use actix_web::error::ErrorBadRequest;
use actix_identity::{Identity};
use std::collections::HashMap;
use actix_web::client::Client;
//use uuid::Uuid;
use futures_util::future::{ok, err, Ready};

//use crate::html;
//use crate::data::Account;

use sqlx::PgPool;
//use actix_session::{Session, CookieSession};


/*
#[derive(Debug, Serialize, Deserialize)]
pub struct User {
    pub id: i32
}

impl FromRequest for User {
    type Error = Error;
    type Future = Ready<Result<Self, Self::Error>>;
    type Config = ();
   
    fn from_request(req: &HttpRequest, payload: &mut dev::Payload) -> Self::Future {

        if let Some(serialized_user) = Identity::from_request(req, payload).into_inner().ok().map(|x|x.identity()).unwrap() {
            let user: User = serde_json::from_str(&serialized_user).unwrap();
            ok(user)
        } else {
            err(ErrorBadRequest("no luck"))
        }
    }
}*/

/*
#[derive(Clone, Debug)]
pub struct OhMySMTP {
    api_url: String,
    token: String 
}

impl OhMySMTP {
    pub fn new(url: &str, token: &str) -> OhMySMTP {
        OhMySMTP{api_url: url.to_owned(), token: token.to_owned()}
    }

    pub async fn send(self: &OhMySMTP, target_mail: &str, login_url: &str) {
        let msg = format!("Your login link: {}", login_url);

        let mut data = HashMap::new();
        data.insert("from", "admin@achims.world");
        data.insert("to", target_mail);
        data.insert("subject", "Login to DogFood");
        data.insert("textbody", &msg);

        let client = Client::default();

        let resp = client.post(&self.api_url)
            .header("OhMySMTP-Server-Token", self.token.to_string())
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .send_json(&data).await.unwrap();
    }
}*/

#[get("/login")]
async fn get_login_form(_: HttpRequest) -> impl Responder {
    html::LoginForm{}
}

#[derive(Deserialize)]
struct LoginFormData {
    email: String,
}

#[get("/logout")]
async fn logout(session: Session, identity: Identity) -> impl Responder {
    identity.forget();
    HttpResponse::Found().header("Location", "/").finish()
}

#[post("/login")]
async fn prepare_login(form: web::Form<LoginFormData>, pool: web::Data<PgPool>, smtp: web::Data<OhMySMTP>) -> impl Responder {
    println!("Email: {}", form.email);

    let attempt = sqlx::query!("select prepare_login($1) as uuid", form.email)
        .fetch_one(pool.get_ref()).await.unwrap().uuid.unwrap();

    let login_url = format!("http://localhost:8088/login/{}", attempt);

    smtp.get_ref().send(&form.email, &login_url).await;


    html::Index{name: "some name"}
}


pub fn config(cfg: &mut web::ServiceConfig) {
    cfg
        .service(get_login_form)
        .service(prepare_login)
        .service(login)
        .service(logout);
}

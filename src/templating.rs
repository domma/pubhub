use tera::Tera;


pub fn load_templates() -> Tera {
    let mut tera = match Tera::new("templates/**/*.html") {
        Ok(t) => t,
        Err(e) => {
            println!("Parsing error(s): {}", e);
            ::std::process::exit(1);
        }
    };
    tera.autoescape_on(vec!["html"]);
//        tera.register_filter("do_nothing", do_nothing_filter);
    tera
}

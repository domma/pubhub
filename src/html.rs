use actix_web::*;
use serde;

use tera::{Tera, Context};


pub struct Render {
    name: String,
    context: Context
}

pub fn render(name: &str) -> Render {
    Render{name: name.to_string(), context: Context::new()}
}

impl Render {
    pub fn data<D: serde::Serialize>(mut self, key: &str, data: D) -> Render {
        self.context.insert(key, &data);
        self
    }
}


impl Responder for Render {
    type Body = String;

    fn respond_to(self, req: &HttpRequest) -> HttpResponse<Self::Body> {
        let tera = req.app_data::<web::Data<Tera>>().unwrap();
        let res = actix_web::HttpResponse::Ok().message_body(
            tera.render(&self.name, &self.context).unwrap()
        );
        res.unwrap()
    }
}

